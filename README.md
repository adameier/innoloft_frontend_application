# Task

## Run app

```
npm install
npm run dev
```

The app should be available at `localhost:1234`

## Additional Tech used

- redux toolkit
- redux-observables (overkill for this app size, but it is my preferred async redux solution)
- formik
- sass
- [gorko](https://github.com/hankchizljaw/gorko) for utility class and token generation
- [CUBE CSS methodology](https://piccalil.li/cube-css/)
- fontawesome icons
- parcel bundler
- prettier
- eslint

## Time taken

About 5h 30min in total
