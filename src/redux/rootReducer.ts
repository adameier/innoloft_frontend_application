import { combineReducers } from 'redux'
import accountInfoReducer from './slices/data/accountInfoSlice'
import userInfoReducer from './slices/data/userInfoSlice'
import accountPanelReducer from './slices/ui/accountPanelSlice'
import userPanelReducer from './slices/ui/userPanelSlice'

const rootReducer = combineReducers({
  data: combineReducers({
    accountInfo: accountInfoReducer,
    userInfo: userInfoReducer
  }),
  ui: combineReducers({
    accountPanel: accountPanelReducer,
    userPanel: userPanelReducer
  })
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
