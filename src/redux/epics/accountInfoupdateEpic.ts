import { createAction } from '@reduxjs/toolkit'
import { AnyAction } from 'redux'
import { exhaustMap, filter, switchMapTo, take } from 'rxjs/operators'
import { Epic } from 'redux-observable'
import { AccountInfoFormData } from '~types'
import { concat, of, timer } from 'rxjs'
import {
  accountInfoRequestLoading,
  accountInfoRequestSuccessful
} from '~redux/slices/ui/accountPanelSlice'
import { setEmail } from '~redux/slices/data/accountInfoSlice'

export const accountInfoUpdateAction = createAction<AccountInfoFormData>('epic/accountInfoUpdate')

const accountInfoUpdateEpic: Epic = (action$) =>
  action$.pipe(
    filter<AnyAction>(accountInfoUpdateAction.match),
    exhaustMap(({ payload }) =>
      concat(
        of(accountInfoRequestLoading()),
        timer(350).pipe(
          take(1),
          switchMapTo(of(setEmail(payload.email), accountInfoRequestSuccessful()))
        )
      )
    )
  )

export default accountInfoUpdateEpic
