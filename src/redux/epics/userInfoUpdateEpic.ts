import { createAction } from '@reduxjs/toolkit'
import { exhaustMap, filter, switchMapTo, take } from 'rxjs/operators'
import { Epic } from 'redux-observable'
import { UserInfo } from '~types'
import { concat, of, timer } from 'rxjs'
import { userInfoRequestLoading, userInfoRequestSuccessful } from '~redux/slices/ui/userPanelSlice'
import { setUserInfo } from '~redux/slices/data/userInfoSlice'

export const userInfoUpdateAction = createAction<UserInfo, 'epic/userInfoUpdate'>(
  'epic/userInfoUpdate'
)

const userInfoUpdateEpic: Epic = (action$) =>
  action$.pipe(
    filter(userInfoUpdateAction.match),
    exhaustMap(({ payload }) =>
      concat(
        of(userInfoRequestLoading()),
        timer(350).pipe(take(1), switchMapTo(of(setUserInfo(payload), userInfoRequestSuccessful())))
      )
    )
  )

export default userInfoUpdateEpic
