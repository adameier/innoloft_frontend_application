import { TypedUseSelectorHook, useSelector } from 'react-redux'
import { RootState } from './rootReducer'

const useRootStateSelector: TypedUseSelectorHook<RootState> = useSelector

export default useRootStateSelector
