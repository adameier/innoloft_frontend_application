import { configureStore } from '@reduxjs/toolkit'
import { combineEpics, createEpicMiddleware } from 'redux-observable'
import accountInfoUpdateEpic from './epics/accountInfoupdateEpic'
import userInfoUpdateEpic from './epics/userInfoUpdateEpic'
import rootReducer from './rootReducer'

const rootEpic = combineEpics(accountInfoUpdateEpic, userInfoUpdateEpic)

const epicMiddleware = createEpicMiddleware()

const store = configureStore({
  reducer: rootReducer,
  middleware: [epicMiddleware]
})

epicMiddleware.run(rootEpic)

export default store
