import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { AccountInfo } from '~types'

const initialState: AccountInfo = {
  email: 'test@test.com'
}

const accountInfoSlice = createSlice({
  name: 'accountInfo',
  initialState,
  reducers: {
    setEmail(state, action: PayloadAction<string>) {
      state.email = action.payload
    }
  }
})

export const { setEmail } = accountInfoSlice.actions

const accountInfoReducer = accountInfoSlice.reducer

export default accountInfoReducer
