import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { UserInfo } from '~types'

const initialState: UserInfo = {
  firstName: 'Homer',
  lastName: 'Simpson',
  houseNumber: '742',
  streetName: 'Evergreen Terrace',
  postalCode: '74821',
  country: 'de'
}

const userInfoSlice = createSlice({
  name: 'userInfo',
  initialState,
  reducers: {
    setUserInfo(_, action: PayloadAction<UserInfo>) {
      return action.payload
    }
  }
})

export const { setUserInfo } = userInfoSlice.actions

const userInfoReducer = userInfoSlice.reducer

export default userInfoReducer
