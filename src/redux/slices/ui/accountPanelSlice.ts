import { createSlice } from '@reduxjs/toolkit'
import { AccountPanelUi } from '~types'

const initialState: AccountPanelUi = {}

const accountPanelSlice = createSlice({
  name: 'accountPanel',
  initialState,
  reducers: {
    accountInfoRequestLoading(state) {
      state.apiRequest = 'loading'
    },
    accountInfoRequestSuccessful(state) {
      state.apiRequest = 'successful'
    }
  }
})

export const { accountInfoRequestSuccessful, accountInfoRequestLoading } = accountPanelSlice.actions

const accountPanelReducer = accountPanelSlice.reducer

export default accountPanelReducer
