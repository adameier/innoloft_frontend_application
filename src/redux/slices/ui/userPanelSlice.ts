import { createSlice } from '@reduxjs/toolkit'
import { UserPanelUi } from '~types'

const initialState: UserPanelUi = {}

const userPanelSlice = createSlice({
  name: 'userPanel',
  initialState,
  reducers: {
    userInfoRequestLoading(state) {
      state.apiRequest = 'loading'
    },
    userInfoRequestSuccessful(state) {
      state.apiRequest = 'successful'
    }
  }
})

export const { userInfoRequestLoading, userInfoRequestSuccessful } = userPanelSlice.actions

const userPanelReducer = userPanelSlice.reducer

export default userPanelReducer
