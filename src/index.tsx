import React from 'react'
import ReactDOM from 'react-dom'
import App from '~App'
import './style/global.scss'

ReactDOM.render(<App />, document.getElementById('react-root'))

if (module.hot) {
  module.hot.accept()
}
