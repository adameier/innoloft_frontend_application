import React, { FC } from 'react'
import Footer from '~components/Footer'
import Header from '~components/Header'
import DrawerNav from '~components/nav/DrawerNav'
import SideNavList from '~components/nav/SideNavList'
import UserDashboard from '~components/dashboard/UserDashboard'
import { Provider } from 'react-redux'
import store from '~redux'

const App: FC = () => (
  <Provider store={store}>
    <Header />
    <main className="gap-top-500">
      <div className="wrapper">
        <div className="has-sidebar">
          <nav>
            <SideNavList />
          </nav>
          <div className="flow">
            <div className="dash-top">
              <DrawerNav />
              <h2 className="color-primary-dark text-700">Dashboard</h2>
            </div>
            <UserDashboard />
          </div>
        </div>
      </div>
    </main>
    <Footer />
  </Provider>
)

export default App
