import React, { FC, useState } from 'react'
import AccountInformationForm from './AccountInformationForm'
import UserInformationForm from './UserInformationForm'

const UserDashboard: FC = () => {
  const [selectedTab, setSelectedTab] = useState<'account' | 'user'>('account')

  return (
    <div className="dash-container">
      <div className="tablist" role="tablist">
        <button
          id="account-tab"
          role="tab"
          aria-selected={selectedTab === 'account'}
          onClick={() => setSelectedTab('account')}
        >
          Account Settings
        </button>
        <button
          id="user-tab"
          role="tab"
          aria-selected={selectedTab === 'user'}
          onClick={() => setSelectedTab('user')}
        >
          User Information
        </button>
      </div>
      <div
        role="tabpanel"
        aria-labelledby="account-tab"
        hidden={selectedTab !== 'account'}
        className="tabpanel"
      >
        <AccountInformationForm />
      </div>
      <div
        role="tabpanel"
        aria-labelledby="user-tab"
        hidden={selectedTab !== 'user'}
        className="tabpanel"
      >
        <UserInformationForm />
      </div>
    </div>
  )
}

export default UserDashboard
