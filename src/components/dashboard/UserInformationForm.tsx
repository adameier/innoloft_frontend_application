import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ErrorMessage, Field, Form, Formik, FormikProps } from 'formik'
import React, { FC, useEffect, useRef, useState } from 'react'
import { useDispatch } from 'react-redux'
import * as yup from 'yup'
import usePrevious from '~hooks/usePrevious'
import { userInfoUpdateAction } from '~redux/epics/userInfoUpdateEpic'
import useRootStateSelector from '~redux/useRootStateSelector'
import { UserInfo } from '~types'

const userInformationSchema = yup
  .object()
  .shape<UserInfo>({
    firstName: yup.string().required('First Name is required'),
    lastName: yup.string().required('Last Name is required'),
    houseNumber: yup.string().required('House Number is required'),
    streetName: yup.string().required('Street Name is required'),
    postalCode: yup.string().required('Postal Code is required'),
    country: yup
      .mixed()
      .oneOf(['de', 'at', 'ch'] as const)
      .required('Country is required')
  })
  .required()

const UserInformationForm: FC = () => {
  const userInfo = useRootStateSelector((s) => s.data.userInfo)
  const request = useRootStateSelector((s) => s.ui.userPanel.apiRequest)
  const prevRequest = usePrevious(request)

  const [flashVisible, setFlashVisible] = useState(false)

  const dispatch = useDispatch()

  useEffect(() => {
    if (request !== prevRequest && request === 'successful') {
      setFlashVisible(true)
    }
  }, [request, prevRequest])

  return (
    <Formik<UserInfo>
      initialValues={{ ...userInfo }}
      validationSchema={userInformationSchema}
      onSubmit={(values) => {
        setFlashVisible(false)
        dispatch(userInfoUpdateAction(values))
      }}
      enableReinitialize={true}
    >
      {({ isValid, values, initialValues }) => (
        <Form className="flow">
          {flashVisible && (
            <div className="flash-message">
              <span>User Information updated successfully</span>
              <button
                className="flash-message_dismiss"
                aria-label="Dismiss this message"
                onClick={() => setFlashVisible(false)}
              >
                <FontAwesomeIcon icon={faTimes} />
              </button>
            </div>
          )}
          <div className="form-item" data-changed={values.firstName !== initialValues.firstName}>
            <label htmlFor="first-name">First Name</label>
            <Field name="firstName" id="first-name" />
            <ErrorMessage component="div" name="firstName" className="form-item_error" />
          </div>
          <div className="form-item" data-changed={values.lastName !== initialValues.lastName}>
            <label htmlFor="last-name">Last Name</label>
            <Field name="lastName" id="last-name" />
            <ErrorMessage component="div" name="lastName" className="form-item_error" />
          </div>
          <div
            className="form-item"
            data-changed={values.houseNumber !== initialValues.houseNumber}
          >
            <label htmlFor="house-number">House Number</label>
            <Field name="houseNumber" id="house-number" />
            <ErrorMessage component="div" name="houseNumber" className="form-item_error" />
          </div>
          <div className="form-item" data-changed={values.streetName !== initialValues.streetName}>
            <label htmlFor="street-name">Street Name</label>
            <Field name="streetName" id="street-name" />
            <ErrorMessage component="div" name="streetName" className="form-item_error" />
          </div>
          <div className="form-item" data-changed={values.postalCode !== initialValues.postalCode}>
            <label htmlFor="postal-code">Postal Code</label>
            <Field name="postalCode" id="postal-code" />
            <ErrorMessage component="div" name="postalCode" className="form-item_error" />
          </div>
          <div className="form-item" data-changed={values.country !== initialValues.country}>
            <label htmlFor="country">Country</label>
            <Field name="country" id="country" as="select">
              <option value="de">Germany</option>
              <option value="at">Austria</option>
              <option value="ch">Switzerland</option>
            </Field>
          </div>
          <button
            type="submit"
            className="form-button"
            disabled={!isValid || request === 'loading'}
          >
            Submit
          </button>
        </Form>
      )}
    </Formik>
  )
}

export default UserInformationForm
