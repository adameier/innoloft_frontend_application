import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ErrorMessage, Field, Form, Formik, FormikProps } from 'formik'
import React, { FC, useEffect, useRef, useState } from 'react'
import { useDispatch } from 'react-redux'
import * as yup from 'yup'
import PasswordStrengthMeter from '~components/PasswordStrengthMeter'
import usePrevious from '~hooks/usePrevious'
import { accountInfoUpdateAction } from '~redux/epics/accountInfoupdateEpic'
import useRootStateSelector from '~redux/useRootStateSelector'
import { AccountInfoFormData } from '~types'

const accountInfoSchema = yup
  .object()
  .shape<AccountInfoFormData>({
    email: yup.string().email('Email is invalid').required('Email is required'),
    newPassword: yup
      .string()
      .min(6, 'Password must be at least 6 characters')
      .matches(
        /^(?:[a-z]|[A-Z]|[0-9]|[*.!@#$%^&(){}[\]:;<>,.?\/~_+\-=|\\])+$/,
        'Password can contain lowercase letters, uppercase letters, digits and any of the following characters: [*.!@#$%^&(){}[]:;<>,.?/~_+-=|]'
      ),
    confirmPassword: yup
      .string()
      .when('newPassword', {
        is: (val: string) => val && val.length > 0,
        then: yup.string().required('Password Confirmation is required')
      })
      .oneOf([yup.ref('newPassword'), null], 'Passwords must match')
  })
  .required()

const AccountInformationForm: FC = () => {
  const email = useRootStateSelector((s) => s.data.accountInfo.email)
  const request = useRootStateSelector((s) => s.ui.accountPanel.apiRequest)
  const prevRequest = usePrevious(request)

  const [flashVisible, setFlashVisible] = useState(false)

  const dispatch = useDispatch()

  useEffect(() => {
    if (request !== prevRequest && request === 'successful') {
      setFlashVisible(true)
    }
  }, [request, prevRequest])

  return (
    <Formik<AccountInfoFormData>
      initialValues={{
        email,
        newPassword: '',
        confirmPassword: ''
      }}
      validationSchema={accountInfoSchema}
      onSubmit={(values) => {
        setFlashVisible(false)
        dispatch(accountInfoUpdateAction(values))
      }}
      enableReinitialize={true}
    >
      {({ isValid, values, initialValues }) => (
        <Form className="flow">
          {flashVisible && (
            <div className="flash-message">
              <span>Account Settings updated successfully</span>
              <button
                className="flash-message_dismiss"
                aria-label="Dismiss this message"
                onClick={() => setFlashVisible(false)}
              >
                <FontAwesomeIcon icon={faTimes} />
              </button>
            </div>
          )}
          <div className="form-item" data-changed={values.email !== initialValues.email}>
            <label htmlFor="email">Email</label>
            <Field name="email" id="email" />
            <ErrorMessage component="div" name="email" className="form-item_error" />
          </div>
          <div className="form-item">
            <label htmlFor="new-password">New Password</label>
            <Field name="newPassword" id="new-password" type="password" />
            <ErrorMessage component="div" name="newPassword" className="form-item_error" />
          </div>
          {values.newPassword && <PasswordStrengthMeter value={values.newPassword} />}
          <div className="form-item">
            <label htmlFor="confirm-password">Confirm New Password</label>
            <Field name="confirmPassword" id="confirm-password" type="password" />
            <ErrorMessage component="div" name="confirmPassword" className="form-item_error" />
          </div>
          <button
            type="submit"
            className="form-button"
            disabled={!isValid || request === 'loading'}
          >
            Submit
          </button>
        </Form>
      )}
    </Formik>
  )
}

export default AccountInformationForm
