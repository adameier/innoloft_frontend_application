import React, { FC } from 'react'

const Footer: FC = () => (
  <footer className="site-footer">
    <div className="wrapper">
      <div className="site-footer_inner">
        <ul className="bottom-nav">
          <li>
            <a href="/#">Contact</a>
          </li>
          <li>
            <a href="/#">Data Privacy</a>
          </li>
          <li>
            <a href="/#">Imprint</a>
          </li>
          <li>
            <a href="/#">Terms of Use</a>
          </li>
          <li>
            <a href="/#">Blog</a>
          </li>
        </ul>
        <p className="text-300 color-grey-dark">© 2020 Innoloft GmbH</p>
      </div>
    </div>
  </footer>
)

export default Footer
