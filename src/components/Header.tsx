import React, { FC } from 'react'

const Header: FC = () => (
  <header role="banner" className="site-header">
    <div className="wrapper">
      <div className="site-header_inner">
        <a className="site-header_logo" href="/#" aria-label="Innoloft - Home">
          <img
            src="https://anvkgjjben.cloudimg.io/width/400/x/https://img.innoloft.de/innoloft-no-white-space.svg"
            alt="Innoloft logo"
          />
        </a>
      </div>
    </div>
  </header>
)

export default Header
