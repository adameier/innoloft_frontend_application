import {
  faBuilding,
  faBullhorn,
  faChartArea,
  faCog,
  faHome,
  faNewspaper
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { FC } from 'react'

const SideNavList: FC = () => (
  <ul className="side-nav">
    <li>
      <a className="side-nav_link" href="/#">
        <FontAwesomeIcon icon={faHome} />
        Home
      </a>
    </li>
    <li>
      <a className="side-nav_link" href="/#">
        <FontAwesomeIcon icon={faBullhorn} />
        My Account
      </a>
    </li>
    <li>
      <a className="side-nav_link" href="/#">
        <FontAwesomeIcon icon={faBuilding} />
        My Company
      </a>
    </li>
    <li>
      <a className="side-nav_link" href="/#">
        <FontAwesomeIcon icon={faCog} />
        My Settings
      </a>
    </li>
    <li>
      <a className="side-nav_link" href="/#">
        <FontAwesomeIcon icon={faNewspaper} />
        News
      </a>
    </li>
    <li>
      <a className="side-nav_link" href="/#">
        <FontAwesomeIcon icon={faChartArea} />
        Analytics
      </a>
    </li>
  </ul>
)

export default SideNavList
