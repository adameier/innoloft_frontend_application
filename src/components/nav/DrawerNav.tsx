import { faBars } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { FC, useState } from 'react'
import SideNavList from './SideNavList'

const DrawerNav: FC = () => {
  const [expanded, setExpanded] = useState(false)

  return (
    <nav className="drawer">
      <button
        className="drawer_button"
        aria-controls="drawer-slider"
        aria-expanded={expanded}
        aria-label="Nav Menu"
        onClick={() => setExpanded(!expanded)}
      >
        <FontAwesomeIcon icon={faBars} />
      </button>
      <div
        className="drawer_bg"
        aria-hidden="true"
        data-show={expanded}
        onClick={() => setExpanded(false)}
      />
      <div id="drawer-slider" className="drawer_slider | pad-top-400" data-open={expanded}>
        <SideNavList />
      </div>
    </nav>
  )
}

export default DrawerNav
