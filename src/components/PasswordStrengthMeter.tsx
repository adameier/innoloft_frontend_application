import React, { FC, useLayoutEffect, useState } from 'react'
import zxcvbn from 'zxcvbn'

function passwordStrengthMsg(score: zxcvbn.ZXCVBNScore) {
  switch (score) {
    case 0:
      return 'Very Weak'
    case 1:
      return 'Weak'
    case 2:
      return 'Average'
    case 3:
      return 'Strong'
    case 4:
      return 'Very Strong'
    default:
      return ''
  }
}

interface Props {
  value: string
}

const PasswordStrengthMeter: FC<Props> = ({ value }) => {
  const [score, setScore] = useState<zxcvbn.ZXCVBNScore>(0)

  useLayoutEffect(() => {
    setScore(zxcvbn(value).score)
  }, [value])

  return (
    <figure className="password-meter">
      <figcaption>Password Strength: {passwordStrengthMsg(score)}</figcaption>
      <div aria-label="Password Strength Visual" data-score={score}></div>
    </figure>
  )
}

export default PasswordStrengthMeter
