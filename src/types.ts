export interface UserInfo {
  firstName: string
  lastName: string
  houseNumber: string
  streetName: string
  postalCode: string
  country: 'de' | 'at' | 'ch'
}

export interface AccountInfo {
  email: string
}

export type AccountInfoFormData = AccountInfo & { newPassword?: string; confirmPassword?: string }

export type ApiRequest = 'loading' | 'failed' | 'successful'
export interface UserPanelUi {
  apiRequest?: ApiRequest
}

export interface AccountPanelUi {
  apiRequest?: ApiRequest
}
